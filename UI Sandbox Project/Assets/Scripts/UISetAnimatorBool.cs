﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UISetAnimatorBool : MonoBehaviour
{
    [SerializeField] Animator _animator;

    public void SetBoolTrue(string paramName){
        _animator.SetBool(paramName, true);
    }

    public void SetBoolFalse(string paramName){
        _animator.SetBool(paramName, false);
    }
}
